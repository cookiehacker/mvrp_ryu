\chapter*{Tinycore}
\addcontentsline{toc}{chapter}{Tinycore}

\begin{definition3}{Définition}
    \textbf{Tiny Core Linux} est une Distribution Linux indépendante, réduite à son minimum 
    (7 à 10 Mo copiés dans la mémoire vive au démarrage), et ayant pour objectif de 
    fournir un système de base à la fois ultra-rapide, léger et évolutif. Tiny Core Linux 
    utilise BusyBox, FLTK, et quelques autres logiciels comparables par leur minimalisme. 
    \cite{tinycore}
\end{definition3}

\begin{figure}[H]
    \centering
    \begin{subfigure}{.50\textwidth}
        \centering
        \begin{center}
            \includegraphics[width=0.7\textwidth]{tiny-core-linux-logo-grand.png}
        \end{center}
        \caption{Logo de tinycore}
        \label{fig:tinycore}
    \end{subfigure}%
    \centering
    \begin{subfigure}{.55\textwidth}
        On a choisi \textbf{Tinycore} afin que chaque membre du groupe puissent librement
        émuler un ordinateur sans soucis de stockage ou de performance. \\

        C'est la distribution linux la plus simple à mettre en place pour réaliser un 
        client virtuel léger, avec uniquement les outils dont on a strictement besoin.
    \end{subfigure}
\end{figure}

On a, à notre disposition 3 différents "cores" : 
\begin{itemize}
    \item \textbf{Core} : Basé sur un système qui propose uniquement une interface 
    en ligne de commande et qui est recommandé pour les utilisateurs expérimentés. Les outils 
    en ligne de commande sont fournis par les extensions que nous pouvons ajouter au 
    système comme par exemple un environnement graphique. Il est idéal pour des serveurs, 
    des appareils peu puissants, ou autre\dots
    \item \textbf{TinyCore} : Recommandé pour les utilisateurs qui ont une connexion
    internet filaire. Il inclut la base du système Core, plus une extension 
    graphique pour utiliser FLTK/FLWM afin d'aider à créer et gérer des interfaces 
    graphiques.
    \item \textbf{CorePlus} : Une image d'installation et non pas une distribution.
    Il est recommandé pour les nouveaux utilisateurs n'ayant pas de connexion internet 
    filaire ou qui n'utilisent pas un clavier US. Il inclut la base du système Core ainsi 
    qu'un outil d'installation qui ne fournit pas les options suivantes : gestion des fenêtres,
    support du sans-fil via plusieurs drivers, support des claviers non US et outils
    revisités. 
\end{itemize}

Dans notre machine tinycore, on aura juste besoin de pouvoir configurer les interfaces 
réseaux ainsi que de pouvoir faire un ping pour tester notre architecture réseau.

\begin{figure}[H]
    \centering
    \begin{subfigure}{.50\textwidth}
        Durant cette partie, on va utiliser Qemu, un logiciel de virtualisation open source.
        Il nous permettra également de réaliser la liaison entre nos machines virtuelles et
        les OVS de façon automatique. De plus, on pourra réaliser divers scénarios comportant
        différentes architectures réseaux en ayant juste à lancer un script. \\

        Qemu utilise l'hyperviseur KVM (Kernel-based Virtual Machine) qui est un 
        hyperviseur libre de type 1 prévu pour Linux.
    \end{subfigure}%
    \begin{subfigure}{.55\textwidth}
        \centering
        \begin{center}
            \includegraphics[width=0.8\textwidth]{Qemu-logo.png}
        \end{center}
        \caption{Logo de Qemu}
        \label{fig:qemu}
    \end{subfigure}
\end{figure}

Durant cette partie, on va réaliser une machine virtuelle sous Tinycore et faire en sorte 
que cette machine soit reliée à l'OVS que nous avons réalisé précédemment.

\pagebreak

\section*{Mise en place de TinyCore}
\addcontentsline{toc}{section}{Mise en place de TinyCore}

On va commencer par créer un disque dur de 1G en format \textit{QCOW} qui signifie 
QEMU Copy On Write, qui utilise une stratégie d'optimisation de stockage sur disque 
qui retarde l'allocation de stockage jusqu'à ce que cela soit réellement nécessaire. 
Pour cela, on peut taper la commande suivante : 

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
qemu-img create -f qcow2 test.qcow2 1G
\end{minted}

On va utiliser l'image tinycore \textit{core.iso} pour nos machines virtuelles :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
wget http://www.tinycorelinux.net/13.x/x86/release/Core-current.iso
\end{minted}

On peut maintenant lancer une machine virtuelle qui démarrera sur l'image tinycore, et utilisant
notre disque dur virtuel : 

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
sudo kvm -m 2048 -smp 2 \
    -net nic,macaddr=00:11:23:EE:EE:EE \
    -hda test.qcow2 \
    -boot order=dc,menu=on \
    -cdrom Core-current.iso 
    -netdev user,id=eth_vm -device e1000,netdev=eth_vm
\end{minted}

\begin{figure}[H]
    \centering
    \begin{center}
        \includegraphics[width=0.8\textwidth]{tinycore.png}
    \end{center}
    \caption{Lancement de Tinycore}
    \label{fig:tinycore2}
\end{figure}

On va maintenant changer la disposition des touches du clavier en installant le paquet 
\textit{kmaps} : 

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
tce-load -wi kmaps
sudo loadkmap < /usr/share/kmap/azerty/fr.kmap
\end{minted}

Puis installer la distribution dans notre disque dur :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
tce-load -wi tc-install
sudo tc-install.sh
\end{minted}

Dès la commande lancée, on rentre dans un installateur, nous proposant les étapes suivantes :
\begin{enumerate}
    \item Localisation de l'OS, ici il est présent sur un cdrom, donc on devra saisir 
    \textbf{c}.
    \item Type de l'installation, on a le choix entre "Frugal" qui consiste à installer le 
    système directement sur le disque dur, puis "HDD" pour les disque dur mobiles, et "Zip"
    pour les sauvegardes. Ici on va utiliser "Frugal" \textbf{f}.
    \item Choix entre utiliser tout le disque dur (1) ou juste une partition (2). Ici on 
    va prendre la première option \textbf{1}. 
    \item Choix du disque dur, ici on prendra le second choix  \textbf{2} : sda.
    \item Demande si on souhaite un "bootloader", et donc oui \textbf{y}
    \item Demande des extensions en plus, ici pas nécessaire \textbf{[ENTRER]}.
    \item Choix de l'option de formatage, ici, on prendra ext4 soit \textbf{3}.
    \item Saisie de l'option de démarrage, ici, ce sera \textbf{tce=sda1}
    \item Demande si l’on veut vraiment formater le disque dur, ici oui \textbf{y}
\end{enumerate}

\pagebreak

\begin{figure}[H]
    \centering
    \begin{center}
        \includegraphics[width=0.7\textwidth]{install.png}
    \end{center}
    \caption{Fin de l'installation de tinycore}
    \label{fig:tinycore3}
\end{figure}

On peut maintenant arrêter la machine et la relancer sans l'image d'installation :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
sudo kvm -m 2048 -smp 2 \
    -net nic,macaddr=00:11:23:EE:EE:EE \
    -hda test.qcow2 \
    -boot order=dc,menu=on \
    -netdev user,id=eth_vm -device e1000,netdev=eth_vm
\end{minted}

On va maintenant utiliser le fichier \textbf{/opt/bootlocal.sh} afin de 
configurer au mieux notre client. Cependant, il ne faudra pas oublier de 
re-télécharger le paquet \textbf{kmaps} :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{bash}
tce-load -wi kmaps
sudo loadkmap < /usr/share/kmap/azerty/fr.kmap
\end{minted}

L'éditeur de fichier mis à disposition dans une telle distribution est 
\textbf{vi}. Il fonctionne tout comme \textit{nano} à quelques exceptions près : 
\begin{itemize}
    \item À l'ouverture d'un fichier, afin de rentrer en mode "édition" ou "\textbf{i}nsertion"
    il faudra appuyer sur la touche \textbf{[i]}.
    \item Pour sortir de ce mode, il faudra appuyer sur la touche \textbf{[ECHAP]}
    \item Pour sauvegarder on tape \textbf{[:w]}
    \item Pour quitter \textbf{[:q]}
    \item Et on peut sauvegarder et quitter à la fois en fusionnant les commandes 
    \textbf{[:wq]}
\end{itemize}

Dans le fichier \textbf{/opt/bootlocal.sh}, on écrira un script qui devra s'exécuter au démarrage de 
la machine en \textbf{root}, on peut simplifier en disant que l'on liste tout simplement 
les commandes que nous voulons. Dans notre cas, on va commencer par la configuration 
de notre clavier, puis la configuration du réseau et pour finir l'activation du transfert 
ip ou "ip forward" pour la suite quand on aura besoin de 2 commutateurs virtuels :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
# Set keyboard
sudo loadkmap < /usr/share/kmap/azerty/fr.kmap

# Set eth0 interface
ifconfig eth0 192.168.1.1 netmask 255.255.255.0

# Enable forwarding
sysctl -w net.ipv4.ip_forward=1
\end{minted}

On doit également taper cette commande afin d'avertir tinycore que le fichier 
\textit{bootlocal.sh} est mis à jours :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
/usr/bin/filetool.sh -b
\end{minted}

\pagebreak

\section*{Connexion des VM à OVS}
\addcontentsline{toc}{section}{Connexion des VM à OVS}

Lors du lancement de notre machine virtuelle, on peut demander à QEMU de configurer 
la ou les cartes réseaux de notre VM.Pour faire en sorte que notre VM soit 
relier à un OVS, on va simplement relier une carte réseau virtuelle que l'on a 
utilisée pour notre OVS à notre VM, cela sera possible avec l'argument 
\textbf{-net}. Ici,on va prendre le même commutateur virtuel que le chapitre 
précédent, et donc lier notre VM au port réseau virtuel \textit{ovs1\_e1}.

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
sudo kvm -m 512 -smp 1 \
    -net nic,macaddr='52:54:00:00:00:01' \
    -net tap,id='ovs1_e1',ifname='ovs1_e1' \
    -hda host1.qcow2 \
    -boot order=dc,menu=on \
    -name host1
\end{minted}

On peut vérifier que cela fonctionne, en créant une seconde VM, en copiant-collant notre 
disque dur virtuel, et en changeant l'adresse IP, dans le fichier 
\textbf{bootlocal.sh}, ainsi que l'adresse MAC que l'on
a saisi dans la commande précédente :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
sudo kvm -m 512 -smp 1 \
    -net nic,macaddr='52:54:00:00:00:02' \
    -net tap,id='ovs1_e2',ifname='ovs1_e2' \
    -hda host2.qcow2 \
    -boot order=dc,menu=on \
    -name host2
\end{minted}

On lancera un \textit{ping} pour tester que tout fonctionne. 
Si on souhaite voir la communication, on peut simplement lancer wireshark et sonder un des 
ports du commutateur.

\begin{figure}[H]
    \centering
    \begin{center}
        \includegraphics[width=1\textwidth]{ovs1wireshark.png}
    \end{center}
    \caption{Scan du réseau sur le port \textit{ovs1\_e1}}
    \label{fig:ovs1e1}
\end{figure}

Maintenant, on va chercher à automatiser cela, on va donc créer un script \textit{bash} 
qui s'occupera de relier notre VM à un OVS, en créant le port, et en y branchant notre 
VM. Typiquement, ces scripts sont nommés \textbf{ovs-ifup.sh} et \textbf{ovs-ifdown.sh}.
Le premier fichier s'occupera de la création du port et la connexion de la VM à ce port,
et le second, au débranchement de la VM à l'OVS et à la suppression du port.
Ces fichiers sont souvent situés dans le dossier \textbf{/etc}. \\

On va donc commencer par le développement de \textbf{/etc/ovs-ifup.sh} :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
#!/bin/bash

switch='ovs1'
ip link set $1 up
ovs-vsctl add-port ${switch} $1
\end{minted}

Puis le développement de \textbf{/etc/ovs-ifdown.sh} :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
#!/bin/bash

switch='ovs1'
ip addr flush dev $1
ip link set $1 down
ovs-vsctl del-port ${switch} $1
\end{minted}

Les valeurs \textbf{\$1} correspondent à la valeur mise en argument au lancement 
du programme, donc typiquement si on tape \mintinline{sh}{sudo ./ovs-ifdown.sh toto},
la valeur \textbf{\$1} sera égale à \textbf{toto}. On découvre que via Qemu, on peut 
lancer ces scripts, via l'argument \textbf{-net}, en sachant que si on saisi une 
interface, il sera mis en argument pour \textbf{ovs-ifup.sh} et \textbf{ovs-ifdown.sh}.


\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
sudo kvm -m 512 -smp 1 \
    -net nic,macaddr='52:54:00:00:00:02' \
    -net tap,id='ovs1_e1',ifname='ovs1_e1',script='/etc/ovs-ifup.sh',downscript='/etc/ovs-ifdown.sh' \
    -hda host1.qcow2 \
    -boot order=dc,menu=on \
    -name host1
\end{minted}

On va maintenant automatiser entièrement, le lancement de notre VM :

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
#!/bin/bash

VM_FOLDER="../VM/"
IFACE="eth0_1"
HOST="host1"
SWITCH="ovs1"
MAC_ADDR=$(printf '52:54:00:%02x:%02x:%02x' $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)))
FILE_UP="/etc/ovs/${SWITCH}-ifup.sh"
FILE_DOWN="/etc/ovs/${SWITCH}-ifdown.sh"
    
sudo kvm -m 512 -smp 1 \
        -net nic,macaddr=${MAC_ADDR} \
        -net tap,id=${IFACE},ifname=${IFACE},script=${FILE_UP},downscript=${FILE_DOWN} \
        -hda ${VM_FOLDER}${HOST}.qcow2 \
        -boot order=dc,menu=on \
        -name ${HOST}    
\end{minted}

En développant ce script, on a décidé que, les fichiers \textbf{ovs-ifup.sh} et 
\textbf{ovs-ifdown.sh} se trouvent dans le dossier \textbf{/etc/ovs}. Et que les VMs
se trouvent dans un dossier \textbf{VM}. Ces scripts permettent de créer des topologies 
réseaux, et donc des scénarios, bien plus facilement. Bien entendu, pour les scénarios,
on retrouve plusieurs machines, donc on a développé un script qui va simplement lancer nos 
machines un à un : 

\begin{minted}[
    framesep=2mm,
    baselinestretch=1.2,xleftmargin=3em,
    fontsize=\footnotesize,linenos]{sh}
#!/bin/bash

NB_HOST=2
echo "Run scenario 1"
echo "Running of $NB_HOST hosts"
    
for (( n=1 ; n<=$NB_HOST ; n++ ));
do
    echo "run Host $n"
    ./runhost$n.sh &
done
\end{minted}
