\contentsline {chapter}{Introduction}{2}{chapter*.2}%
\contentsline {chapter}{Virtual LAN (VLAN)}{3}{chapter*.3}%
\contentsline {section}{Réalisation de VLANs et d'un trunk}{4}{figure.caption.4}%
\contentsline {section}{Conclusion}{6}{figure.caption.6}%
\contentsline {chapter}{OpenVirtualSwitch (OVS)}{7}{chapter*.7}%
\contentsline {section}{Installation de OpenVirtualSwitch}{8}{figure.caption.9}%
\contentsline {section}{Mise en place de notre premier switch virtuel}{9}{figure.caption.9}%
\contentsline {chapter}{Tinycore}{10}{chapter*.13}%
\contentsline {section}{Mise en place de TinyCore}{11}{figure.caption.15}%
\contentsline {section}{Connexion des VM à OVS}{13}{figure.caption.17}%
\contentsline {chapter}{Utilisation des VLANs avec OVS}{15}{chapter*.19}%
\contentsline {section}{Création d'une architecture simple}{16}{chapter*.19}%
\contentsline {section}{Connexion des VM}{17}{figure.caption.20}%
\contentsline {chapter}{Ryu}{18}{chapter*.22}%
\contentsline {section}{Notre premier programme Ryu}{19}{figure.caption.23}%
\contentsline {chapter}{OpenFlow}{21}{chapter*.25}%
\contentsline {section}{Un paquet OpenFlow}{22}{chapter*.25}%
\contentsline {section}{Communication OpenFlow}{24}{table.0.3}%
\contentsline {chapter}{OpenVirtualSwitch Database (OVSDB)}{25}{chapter*.26}%
\contentsline {section}{Interaction avec OVSDB}{26}{figure.caption.27}%
\contentsline {chapter}{REST API}{27}{chapter*.29}%
\contentsline {section}{Utilisation de curl}{29}{figure.caption.30}%
\contentsline {chapter}{Multiple VLAN Registration (MVRP)}{31}{chapter*.32}%
\contentsline {chapter}{Environnement de travail}{32}{chapter*.34}%
\contentsline {section}{Mise en place d'un environnement python}{33}{figure.caption.35}%
\contentsline {chapter}{Lots}{34}{chapter*.37}%
\contentsline {section}{Lot 1}{35}{figure.caption.38}%
\contentsline {subsection}{Mise en place de l'architecture réseau}{35}{figure.caption.39}%
\contentsline {subsection}{Mise en place du contrôleur}{37}{figure.caption.42}%
\contentsline {subsection}{Communication avec la base OVSDB}{38}{figure.caption.42}%
\contentsline {subsection}{Interaction avec l'API via un programme python}{39}{figure.caption.43}%
\contentsline {subsection}{Test de l'application}{40}{figure.caption.44}%
\contentsline {section}{Lot 2}{41}{figure.caption.45}%
\contentsline {subsection}{Mise en place de l'architecture réseau}{41}{figure.caption.45}%
\contentsline {subsection}{Mise en place du contrôleur}{43}{figure.caption.45}%
\contentsline {subsection}{Test de l'application}{44}{figure.caption.46}%
\contentsline {section}{Lot 3}{45}{figure.caption.48}%
\contentsline {subsection}{Mise en place du contrôleur}{45}{figure.caption.49}%
