# MVRP_ryu

## Parties fonctionnelles
- API REST
- Un début de MVRP
- Démarrage des VMs + OVS
- Utilisation d'OpenFlow pour la détections de nouveaux ports, de switchs, ...
	
## Parties non fonctionnelles
- Utilisation d'OpenFlow pour modifier les VLANs / trunks

## Si projet repris
Bien mieux comprendre OpenFlow + chercher à intéragir avec les équipements réseaux via le protocole OpenFlow et sans OVSDB
