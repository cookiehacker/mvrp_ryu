from ryu.lib.ovs import vsctl
import os
import sys



class Topology:
    TIMEOUT = 1
    HOST = "127.0.0.1"
    PORT = "6640"


    def __init__(self):
        self.header()

    def header(self):
        print("\n==========================\n")

        print("\033[1;34m%s\n%s\n%s\n%s\n%s\n%s\n\n%s\033[0m" % (
                    "         ----- (  ()",
                    "       ' o      (   ()",
                    "      '     o    ( ()",
                    "      |  o     o | ",
                    "       '   o  o ' ",
                    "         ------  ",
                    "VLAN PROJECT (Topology)"
                ))

        print("\n==========================\n")

    def run(self):
        self.input_server()

        

    def serverConnection(self, ip, port):
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        sock.settimeout(self.TIMEOUT)
        result = sock.connect_ex((ip,int(port)))
        status = result == 0
        sock.close()
        return status

    def input_server(self):
        print("\nPlease enter a IP address [127.0.0.1] :")
        host=input("> ")

        print("\nPlease enter port [6640] :")
        port=input("> ")

        if(host != ''): self.HOST = host
        if(port != ''): self.PORT = port

        connection_status = self.serverConnection(self.HOST,self.PORT)
        print("\nConnection to %s:%s : %s" % (self.HOST,self.PORT,"\033[1;32mSUCCESS\033[0m" if connection_status else "\033[1;31mFAILED\033[0m"))
        if not connection_status: sys.exit(1)        
    

if __name__ == '__main__':
    instance = Topology()
    instance.run()