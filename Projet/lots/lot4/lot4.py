# -*- coding: utf-8 -*-

import json

from ryu.base import app_manager

from ryu.controller import ofp_event
from ryu.controller import dpset
from ryu.controller import controller
from ryu.controller import conf_switch
from ryu.controller import network

from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER, HANDSHAKE_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls, set_ev_handler

from ryu.ofproto import ofproto_v1_3

from ryu.lib.mac import haddr_to_bin

from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import vlan

from ryu.lib.dpid import dpid_to_str

from ryu.lib.ovs import vsctl
from ryu.lib.ovs import bridge

from ryu.topology.api import get_switch

from ryu.app.wsgi import ControllerBase
from ryu.app.wsgi import Response
from ryu.app.wsgi import route
from ryu.app.wsgi import WSGIApplication

from ryu.ofproto import ether

from ryu.lib import dpid as dpid_lib

class lot_controller(ControllerBase):

    DPID_PATTERN = dpid_lib.DPID_PATTERN

    def __init__(self, req, link, data, **config):
        super(lot_controller, self).__init__(req, link, data, **config)
        self.lot4 = data['lot4']

    @route('topology', '/switches',
           methods=['GET'])
    def list_switches(self, req, **kwargs):
        return self._switches(req, **kwargs)

    @route('topology', '/switches/{dpid}',
           methods=['GET'], requirements={'dpid': DPID_PATTERN})
    def get_switch(self, req, **kwargs):
        return self._switches(req, **kwargs)

    def _switches(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        switches = get_switch(self.lot4, dpid)
        body = json.dumps([switch.to_dict() for switch in switches])
        return Response(content_type='application/json', body=body)


class lot4(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
        'wsgi': WSGIApplication
    }

    def __init__(self, *args, **kwargs):
        super(lot4, self).__init__(*args, **kwargs)
        wsgi = kwargs['wsgi']
        wsgi.register(lot_controller, {'lot4': self})

        self.header()

    def header(self):
        print("\n==========================\n")

        print("\033[1;34m%s\n%s\n%s\n%s\n%s\n%s\n\n%s\033[0m" % (
            "         ----- (  ()",
            "       ' o      (   ()",
            "      '     o    ( ()",
            "      |  o     o | ",
            "       '   o  o ' ",
            "         ------  ",
            "VLAN PROJECT (VLAN detector)"
        ))

        print("\n==========================\n")       


    def send_flow_mod(self, datapath):
        ofp = datapath.ofproto
        ofp_parser = datapath.ofproto_parser

        cookie = cookie_mask = 0
        table_id = 0
        idle_timeout = hard_timeout = 0
        priority = 32768
        buffer_id = ofp.OFP_NO_BUFFER

        actions = [
            ofp_parser.OFPActionPushVlan(ether.ETH_TYPE_8021Q),
            ofp_parser.OFPActionSetField(vlan_vid=42)
        ]
        match = ofp_parser.OFPMatch(vlan_vid=0x0000)
        inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,
                                             actions)]

        req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,
                                table_id, ofp.OFPFC_ADD,
                                idle_timeout, hard_timeout,
                                priority, buffer_id,
                                ofp.OFPP_ANY, ofp.OFPG_ANY,
                                ofp.OFPFF_SEND_FLOW_REM,
                                match, inst)
        
        datapath.send_msg(req)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(dpset.EventDP, MAIN_DISPATCHER)
    def _switch_status(self, ev):
        datapath = ev.dp
        enter = ev.enter
        ports = ev.ports
        dpid = datapath.id

        if(enter):
            print("Hello : "+str(dpid))
            self.send_flow_mod(datapath)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        msg         = ev.msg
        datapath    = msg.datapath
        ofproto     = datapath.ofproto
        parser      = datapath.ofproto_parser

        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):        
        msg         = ev.msg
        datapath    = msg.datapath
        ofproto     = datapath.ofproto
        parser      = datapath.ofproto_parser
        port_no     = msg.match['in_port']
        
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data
        
        out = parser.OFPPacketOut(
            datapath=datapath, buffer_id=msg.buffer_id, in_port=port_no,
            actions=[], data=data)

        datapath.send_msg(out)