import requests 
import socket
import json
import os
import sys
import re

class VlanValueError(Exception):
    pass

class Port:
    def __init__(self,dpid,port_no,hw_addr,name,vlan):
        self.dpid = dpid
        self.port_no = port_no
        self.hw_addr = hw_addr
        self.name = name
        self.vlan = vlan
    
    def get_dpid(self):
        return self.dpid

    def get_port_no(self):
        return self.port_no

    def get_hw_addr(self):
        return self.hw_addr

    def get_name(self):
        return self.name

    def get_vlan(self):
        return self.vlan    

    def set_vlan(self,vlan):
        self.vlan = vlan

    def set_dpid(self,dpid):
        self.dpid = dpid

    def set_port_no(self,port_no):
        self.port_no = port_no

    def set_hw_addr(self,hw_addr):
        self.hw_addr = hw_addr

    def set_name(self,name):
        self.name = name

class EditVlan:
    TIMEOUT=1
    HOST = "127.0.0.1"
    PORT = "8080"
    ports = []
    INT_PATTERN = r'[0-9]*'

    def __init__(self):
        self.header()
    

    def header(self):
        print("\n==========================\n")

        print("\033[1;34m%s\n%s\n%s\n%s\n%s\n%s\n\n%s\033[0m" % (
                    "         ----- (  ()",
                    "       ' o      (   ()",
                    "      '     o    ( ()",
                    "      |  o     o | ",
                    "       '   o  o ' ",
                    "         ------  ",
                    "VLAN PROJECT (vlanEditor)"
                ))

        print("\n==========================\n")

    def run(self):
        loop = True

        self.input_server()
        print("\nChoose the port that do you want edit :")
        self.serialize_ports()
        self.print_port_list()

        while(loop):
            p = self.input_port()
            print("\nThe port selected : \033[1;36m%s\033[0m in vlan \033[1;31m%d\033[0m\nSelect a new vlan :" % (p.get_name(), p.get_vlan()))
            v = self.input_vlan()
            result = self.apply_change(p, v)
            print("\nVLAN modification : %s" % ("\033[1;32mSUCCESS\033[0m" if(result["code"] == 0) else "\033[1;31mFAILED\033[0m"))
            self.updatePort()
            self.print_port_list()

    def serverConnection(self, ip, port):
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        sock.settimeout(self.TIMEOUT)
        result = sock.connect_ex((ip,int(port)))
        status = result == 0
        sock.close()
        return status

    def apply_change(self,port,vlan):
        return requests.post("http://%s:%s/access" % (self.HOST, self.PORT), json={"portname": port.get_name(), "vlanID": vlan}).json()


    def input_vlan(self):
        loop = True

        while(loop):
            vlan = input("> ")

            try:
                v = int(vlan)
                if(not(0 < v <= 4096)): raise VlanValueError
                return v
            except ValueError:
                print("\033[1;31m[ERROR] The value must be int\033[0m")
            except VlanValueError:
                print("\033[1;31m[ERROR] The value must be between 1 and 4096\033[0m")

    def input_port(self):
        loop = True

        while(loop):
            port = input("> ")

            try:
                p = int(port)
                return self.ports[p]
            except ValueError:
                print("\033[1;31m[ERROR] The value must be int\033[0m")
            except IndexError:
                print("\033[1;31m[ERROR] Index out of range\033[0m")
        

    def input_server(self):
        print("\nPlease enter a IP address [127.0.0.1] :")
        host=input("> ")

        print("\nPlease enter port [8080] :")
        port=input("> ")

        if(host != ''): self.HOST = host
        if(port != ''): self.PORT = port

        connection_status = self.serverConnection(self.HOST,self.PORT)
        print("\nConnection to %s:%s : %s" % (self.HOST,self.PORT,"\033[1;32mSUCCESS\033[0m" if connection_status else "\033[1;31mFAILED\033[0m"))
        if not connection_status: sys.exit(1)
        
    def get_ports(self):
        return (requests.get("http://%s:%s/access" % (self.HOST, self.PORT)).json())

    def updatePort(self):
        self.ports = []
        self.serialize_ports()

    def serialize_ports(self):
        ports_json = self.get_ports()
        for port in ports_json:
            p = Port(port['dpid'], port['port_no'], port['hw_addr'], port['name'], port['vlan'])
            self.ports.append(p)

    def print_port_list(self):
        cpt = 0
        for port in self.ports:
            print("     \033[1;32m>\033[0m [%d] \033[1;36m%s\033[0m in vlan \033[1;31m%d\033[0m" % (cpt, port.get_name(), port.get_vlan()))
            cpt += 1

if __name__ == '__main__':
    instance = EditVlan()
    instance.run()