# -*- coding: utf-8 -*-

import json

from ryu.base import app_manager

from ryu.controller import ofp_event
from ryu.controller import dpset
from ryu.controller import controller
from ryu.controller import conf_switch
from ryu.controller import network

from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER, HANDSHAKE_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls, set_ev_handler

from ryu.ofproto import ofproto_v1_3

from ryu.lib.mac import haddr_to_bin

from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import vlan

from ryu.lib.dpid import dpid_to_str

from ryu.lib.ovs import vsctl
from ryu.lib.ovs import bridge

from ryu.topology.api import get_switch

from ryu.app.wsgi import ControllerBase
from ryu.app.wsgi import Response
from ryu.app.wsgi import route
from ryu.app.wsgi import WSGIApplication

from ryu.lib import dpid as dpid_lib

class lot_controller(ControllerBase):

    OVSDB_ADDR = 'tcp:127.0.0.1:6640'
    VLANID_PATTERN = r'[0-9]{1,4}|all'
    DPID_PATTERN = dpid_lib.DPID_PATTERN
    PORTNAME_PATTERN = r'([a-z]|[0-9])*'

    def __init__(self, req, link, data, **config):
        super(lot_controller, self).__init__(req, link, data, **config)
        self.lot2 = data['lot2']
        self.confOVSDB = data["confOVSDB"]

    @route('topology', '/switches',
           methods=['GET'])
    def list_switches(self, req, **kwargs):
        return self._switches(req, **kwargs)

    @route('topology', '/switches/{dpid}',
           methods=['GET'], requirements={'dpid': DPID_PATTERN})
    def get_switch(self, req, **kwargs):
        return self._switches(req, **kwargs)

    @route('topology', '/access', methods=['GET'])
    def list_access_of_switchs(self, req, **kwargs):
        return self._access(req, **kwargs)

    @route('topology', '/access/{dpid}', methods=['GET'], requirements={'dpid': DPID_PATTERN})
    def list_access_of_switch(self, req, **kwargs):
        return self._access(req, **kwargs)

    @route('topology', '/access',
           methods=['POST'], requirements={'portname': PORTNAME_PATTERN, 'vlanID': VLANID_PATTERN, 'dpid': DPID_PATTERN})
    def set_vlan_access(self, req, **kwargs):
        return self._set_vlan(req, **kwargs)

    @route('topology', '/trunk', methods=['GET'])
    def list_trunk_of_switchs(self, req, **kwargs):
        return self._trunk(req, **kwargs)

    @route('topology', '/trunk{dpid}', methods=['GET'], requirements={'dpid': DPID_PATTERN})
    def list_trunk_of_switch(self, req, **kwargs):
        return self._trunk(req, **kwargs)

    def _switches(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        switches = get_switch(self.lot2, dpid)
        body = json.dumps([switch.to_dict() for switch in switches])
        return Response(content_type='application/json', body=body)

    def _access(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        switches = get_switch(self.lot2, dpid)
        bovs = bridge.OVSBridge(CONF=self.confOVSDB,
                                datapath_id=dpid, ovsdb_addr=self.OVSDB_ADDR)
        ports = []
        for s in switches:
            for p in s.to_dict()["ports"]:
                vlanList = bovs.get_db_attribute("Port", p['name'], "tag")
                if len(vlanList) > 0:
                    vlanID = vlanList[0]
                    p['vlan'] = vlanID
                    ports.append(p)
        body = json.dumps(ports)
        return Response(content_type='application/json', body=body)

    def _trunk(self, req, **kwargs):
        dpid = None
        if 'dpid' in kwargs:
            dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        switches = get_switch(self.lot2, dpid)
        bovs = bridge.OVSBridge(CONF=self.confOVSDB,
                                datapath_id=dpid, ovsdb_addr=self.OVSDB_ADDR)
        ports = []
        for s in switches:
            for p in s.to_dict()["ports"]:
                trunkList = bovs.get_db_attribute("Port", p['name'], "trunks")
                if len(trunkList) > 0:
                    p['trunk'] = trunkList
                    ports.append(p)
        body = json.dumps(ports)
        return Response(content_type='application/json', body=body)


    def _set_vlan(self, req, **kwargs):
        portname    = ''
        vlanID      = 0
        request     = req.json
        switches    = get_switch(self.lot2, None)

        bovs = bridge.OVSBridge(CONF=self.confOVSDB,
                               datapath_id=None, ovsdb_addr=self.OVSDB_ADDR)
        
        #bovs.init()
        if vsctl.valid_ovsdb_addr(self.OVSDB_ADDR):
            ovs_vsctl = vsctl.VSCtl(self.OVSDB_ADDR)

            if len({'portname', 'vlanID'}.difference(set(request))) == 0:
                portname = request['portname']
                vlanID = request['vlanID']
                
                bovs.set_db_attribute("Port",portname,"tag",vlanID)
                allVLAN = set()

                for s in switches:
                    for p in s.to_dict()["ports"]:
                        vlanList = bovs.get_db_attribute("Port", p['name'], "tag")
                        if len(vlanList) > 0:
                            allVLAN.add(str(vlanList[0]))

                for s in switches:
                    for p in s.to_dict()["ports"]:
                        trunkList = bovs.get_db_attribute("Port", p['name'], "trunks")
                        if len(trunkList) > 0:
                            trunkList = []
                            trunks = ",".join(allVLAN)
                            bovs.set_db_attribute("Port",p['name'],"trunks",trunks)

                body = json.dumps(
                    {"error": "SUCCESS", "code":0, "elems": request})
                return Response(content_type='application/json', body=body)
            else:
                body = json.dumps(
                    {"error": "NOT ALL VALUES FOUND", "code":1, "elems": request})
                return Response(content_type='application/json', body=body)
        else:
            body = json.dumps(
                    {"error": "IP not valid", "code":3, "elems": self.OVSDB_ADDR})
            return Response(content_type='application/json', body=body)


class lot2(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
        'wsgi': WSGIApplication
    }

    def __init__(self, *args, **kwargs):
        super(lot2, self).__init__(*args, **kwargs)
        wsgi = kwargs['wsgi']
        wsgi.register(lot_controller, {'lot2': self, 'confOVSDB': self.CONF})

        self.header()

    def header(self):
        print("\n==========================\n")

        print("\033[1;34m%s\n%s\n%s\n%s\n%s\n%s\n\n%s\033[0m" % (
            "         ----- (  ()",
            "       ' o      (   ()",
            "      '     o    ( ()",
            "      |  o     o | ",
            "       '   o  o ' ",
            "         ------  ",
            "VLAN PROJECT (VLAN detector)"
        ))

        print("\n==========================\n")

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath    = ev.msg.datapath
        ofproto     = datapath.ofproto
        parser      = datapath.ofproto_parser

        # install table-miss flow entry

        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):        
        msg         = ev.msg
        datapath    = msg.datapath
        ofproto     = datapath.ofproto
        parser      = datapath.ofproto_parser
        port_no     = msg.match['in_port']
        
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data
        
        out = parser.OFPPacketOut(
            datapath=datapath, buffer_id=msg.buffer_id, in_port=port_no,
            actions=[], data=data)

        datapath.send_msg(out)