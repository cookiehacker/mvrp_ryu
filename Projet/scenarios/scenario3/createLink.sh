#!/bin/bash

# Create p0 port for first switch
sudo ovs-vsctl add-port ovs1 p0 trunks=1 -- set Interface p0 type=patch -- set interface p0 options:peer=p1

# Create p1 port for second switch
sudo ovs-vsctl add-port ovs2 p1 trunks=1 -- set Interface p1 type=patch -- set interface p1 options:peer=p0
