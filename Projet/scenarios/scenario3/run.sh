#!/bin/bash

read -p $'\e[1;34mStart simulation [ENTER]\e[0m'
# clear switchs
sudo ./deleteAll.sh

read -p $'\e[1;34mCreate first switch [ENTER]\e[0m'
# create switchs
sudo ./runOVS1.sh

read -p $'\e[1;34mCreate second switch [ENTER]\e[0m'
sudo ./runOVS2.sh

read -p $'\e[1;34mStart VMs of first switch [ENTER]\e[0m'
sudo ./runhostsovs1.sh

read -p $'\e[1;34mStart VMs of second switch [ENTER]\e[0m'
sudo ./runhostsovs2.sh

read -p $'\e[1;34mCreate patch cable [ENTER]\e[0m'
sudo ./createLink.sh

read -p $'\e[1;34mRemove second switch [ENTER]\e[0m'
sudo ovs-vsctl del-br ovs2

read -p $'\e[1;34mRemove first switch [ENTER]\e[0m'
sudo ovs-vsctl del-br ovs1