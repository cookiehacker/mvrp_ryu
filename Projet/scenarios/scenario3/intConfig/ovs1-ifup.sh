#!/bin/bash

netdev=$1
switch="ovs1"
ip link set $netdev up
ovs-vsctl add-port ${switch} $netdev tag=1
