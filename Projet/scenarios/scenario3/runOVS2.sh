#!/bin/bash

# Create connection to OVSDB
sudo ovs-vsctl set-manager "ptcp:6640"

# Create SWITCH
sudo ovs-vsctl add-br ovs2 -- set Bridge ovs2 fail-mode=secure

# sudo ovs-vsctl add-port ovs2 ovs2_e1 -- set Interface ovs2_e1 type=internal
# sudo ovs-vsctl add-port ovs2 ovs2_e2 -- set Interface ovs2_e2 type=internal

# Connection to controller
sudo ovs-vsctl set-controller ovs2 tcp:127.0.0.1:6633

# Clear flows table
sudo ovs-ofctl del-flows ovs2

# Configure OFP for fist switch
sudo ovs-ofctl add-flow --protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14 ovs2 actions=normal,controller

