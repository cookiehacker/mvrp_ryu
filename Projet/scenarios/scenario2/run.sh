#!/bin/bash

# clear switchs
sudo ./deleteAll.sh

# create switchs
sudo ./runOVS.sh

# run Hosts
NB_HOST=4
echo "Run scenario 2"
echo "Running of $NB_HOST hosts"

for (( n=1 ; n<=$NB_HOST ; n++ ));
do
    echo "run Host $n"
    ./runhost$n.sh &
done