#!/bin/bash

VM_FOLDER="../VM/"
IFACE="ovs1_e2"
HOST="host2"
SWITCH="ovs1"
MAC_ADDR=$(printf '52:54:00:%02x:%02x:%02x' $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)))
FILE_UP="./intConfig/${SWITCH}-ifup.sh"
FILE_DOWN="./intConfig/${SWITCH}-ifdown.sh"

sudo kvm -m 512 -smp 1 \
	-net nic,macaddr=${MAC_ADDR} \
	-net tap,id=${IFACE},ifname=${IFACE},script=${FILE_UP},downscript=${FILE_DOWN} \
	-hda ${VM_FOLDER}${HOST}.qcow2 \
	-boot order=dc,menu=on \
	-name ${HOST}
