#!/bin/bash

# clear switchs
sudo ./deleteAll.sh

# create switchs
sudo ./runOVS.sh

# run Hosts
NB_HOST=2
echo "Run scenario 1"
echo "Running of $NB_HOST hosts"

for (( n=1 ; n<=$NB_HOST ; n++ ));
do
    echo "run Host $n"
    ./runhost$n.sh &
done