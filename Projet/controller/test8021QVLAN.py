# -*- coding: utf-8 -*-

from ryu.base import app_manager

from ryu.controller import ofp_event 

from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls

from ryu.ofproto import ofproto_v1_3

from ryu.lib.mac import haddr_to_bin

from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import vlan

from ryu.lib.dpid import dpid_to_str

from ryu.lib.ovs import vsctl
from ryu.lib.ovs import bridge

from ryu.topology.api import get_switch

class vlanDetector(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]    

    def __init__(self, *args, **kwargs):
        super(vlanDetector, self).__init__(*args, **kwargs)
        self.header()
        
    def header(self):
        print("\n==========================\n")

        print("\033[1;34m%s\n%s\n%s\n%s\n%s\n%s\n\n%s\033[0m"%(
            "         ----- (  ()",
            "       ' o      (   ()",
            "      '     o    ( ()",
            "      |  o     o | ",
            "       '   o  o ' ",
            "         ------  ",
            "VLAN PROJECT (VLAN detector)"
        ))

        print("\n==========================\n")

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry

        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg         = ev.msg
        datapath    = msg.datapath
        ofproto     = datapath.ofproto
        parser      = datapath.ofproto_parser
        dpid        = datapath.id
        port_no     = msg.match['in_port']
        pkt         = packet.Packet(msg.data)
        eth         = pkt.get_protocols(ethernet.ethernet)[0]
        vlan_header = pkt.get_protocols(vlan.vlan)

        self.logger.info(vlan_header)
    

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data
        
        out = parser.OFPPacketOut(
            datapath=datapath, buffer_id=msg.buffer_id, in_port=port_no,
            actions=[], data=data)

        datapath.send_msg(out)