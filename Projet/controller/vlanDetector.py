# -*- coding: utf-8 -*-

from ryu.base import app_manager

from ryu.controller import ofp_event 

from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls

from ryu.ofproto import ofproto_v1_3

from ryu.lib.mac import haddr_to_bin

from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import vlan

from ryu.lib.dpid import dpid_to_str

from ryu.lib.ovs import vsctl
from ryu.lib.ovs import bridge

from ryu.topology.api import get_switch

class vlanDetector(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    OVSDB_ADDR = 'tcp:127.0.0.1:6640'
    

    def __init__(self, *args, **kwargs):
        super(vlanDetector, self).__init__(*args, **kwargs)
        self.header()
        
    def header(self):
        print("\n==========================\n")

        print("\033[1;34m%s\n%s\n%s\n%s\n%s\n%s\n\n%s\033[0m"%(
            "         ----- (  ()",
            "       ' o      (   ()",
            "      '     o    ( ()",
            "      |  o     o | ",
            "       '   o  o ' ",
            "         ------  ",
            "VLAN PROJECT (VLAN detector)"
        ))

        print("\n==========================\n")

    # def findPortByNumber(self, portNumber, listOfPort):
    #     for port in listOfPort:
    #         if int(port["port_no"],16) == portNumber:
    #             return port
    #     return None

    # def findPortByHw(self,hw,listOfPort):
    #     for port in listOfPort:
    #         if port["src"] == hw:
    #             return port
    #     return None

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    # @set_ev_cls(ofp_event.EventOFPPortStateChange, MAIN_DISPATCHER)
    # def _switch_ports_handler(self,ports):
    #     self.logger.info(
    #         "\033[1;32mThe port\033[0m\033[1;33m %s \033[0m\033[1;32mhave changed for this reason : %s \033[0m",
    #         ports.port_no,
    #         ports.reason
    #     )
        

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry

        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    # @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    # def _packet_in_handler(self, ev):        
    #     msg         = ev.msg
    #     datapath    = msg.datapath
    #     ofproto     = datapath.ofproto
    #     parser      = datapath.ofproto_parser
    #     dpid        = datapath.id
    #     switch      = get_switch(self, dpid)[0].to_dict()
    #     port_no     = msg.match['in_port']
    #     bovs        = bridge.OVSBridge(CONF = self.CONF, datapath_id = dpid, ovsdb_addr = self.OVSDB_ADDR)
    #     pkt         = packet.Packet(msg.data)
    #     eth         = pkt.get_protocols(ethernet.ethernet)[0]
    #     dst         = eth.dst
    #     src         = eth.src
    #     portsrc     = self.findPortByNumber(port_no,switch["ports"])
    #     #portdst     = self.findPortByHw(dst, switch["ports"])
    #     vlanID      = 0

    #     if(portsrc != None):
    #         getVlan = bovs.get_db_attribute("Port",portsrc['name'],"tag")
    #         vlanID = getVlan[0] if len(getVlan) > 0 else 0 
    #         self.logger.info("\033[1;32m[%s]\033[0m packet %s, in VLAN %s, from port %s (%s)", 
    #             str(type(pkt.protocols[-1])).split('.')[-1].replace('\'>',''),
    #             eth, 
    #             vlanID if vlanID != 0 else "\033[1;34m[NO VLAN]\033[0m",
    #             msg.match['in_port'],
    #             portsrc['name'] if portsrc != None else "\033[1;33m[NOT FOUND]\033[0m"
    #         )

    #     actions = [
    #         parser.OFPActionOutput(ofproto.OFPP_FLOOD,0)
    #     ]
        
    #     data = None
    #     if msg.buffer_id == ofproto.OFP_NO_BUFFER:
    #         data = msg.data
        
    #     out = parser.OFPPacketOut(
    #         datapath=datapath, buffer_id=msg.buffer_id, in_port=port_no,
    #         actions=actions, data=data)

    #     datapath.send_msg(out)